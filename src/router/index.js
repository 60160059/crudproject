import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/ข้อมูลอบรมสหกิจ',
    name: 'ข้อมูลอบรมสหกิจ',
    component: () => import('../views/Users')
  },
  {
    path: '/ชั่วโมงอบรมสหกิจ',
    name: 'ชั่วโมงอบรมสหกิจ',
    component: () => import('../views/Hours')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
